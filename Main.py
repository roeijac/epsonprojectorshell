import sys,urllib2,base64
from time import sleep
__author__ = 'Roei Jacobovich'


def getCookie():
    req = urllib2.Request('http://' + str(host) + '/webconf/index2.html')
    base64string = base64.encodestring('%s:%s' % ('EPSONWEB', passw))[:-1]
    req.add_header("Authorization", "Basic %s" % base64string)
    res = urllib2.urlopen(req)
    return res.headers.headers[2][12:-10]


def help():
    print 'List of all functions available:'
    print 'PWR:\tPower Button (Execute twice to turn off projector)'
    print 'SRC:\tSource Search'
    print 'COMP:\tSource is Computer'
    print 'VIDEO:\tSource is Video'
    print 'USB:\tSource is USB'
    print 'LAN:\tSource is LAN'
    print 'MUTE:\tA/V Mute'
    print 'PAUSE:\tPause'
    print 'VOLUP:\tVolume Up'
    print 'VOLDOWN:\tVolume Down'
    print 'PAGEUP:\tPage Up'
    print 'PAGEDOWN:\tPage Down'


def parseCmd(cmd):
    switcher = {
        'PWR': '3B',
        'SRC':'67',
        'COMP':'43',
        'VIDEO':'46',
        'USB':'85',
        'LAN':'8A',
        'MUTE':'3E',
        'PAUSE':'47',
        'VOLUP':'56',
        'VOLDOWN':'57',
        'PAGEUP':'68',
        'PAGEDOWN':'69'
    }
    return switcher.get(cmd, "NONE")


def hack():
    print "Weak Password My Friends.."
    while 1:
        send('LAN')
        sleep(4)
        send('COMP')
        sleep(4)
        send('USB')
        sleep(4)
        send('VIDEO')
        sleep(4)


def send(cmd):
    if cmd == 'HELP':
        help()
        return
    if cmd == 'HACK':
        hack()
        return
    key = parseCmd(cmd)
    if key != 'NONE':
        req = urllib2.Request('http://' + str(host) + '/cgi-bin/directsend?KEY=' + key)
        req.add_header("Cookie", cookie)
        req.add_header("Referer",'http://'+str(host)+'/cgi-bin/webconf.exe?page=13')
        urllib2.urlopen(req)


def main():
    global host,passw,cookie
    print 'Welcome to Epson Projector Shell'
    if len(sys.argv) == 3:
        host = sys.argv[1]
        passw = sys.argv[2]
    elif len(sys.argv) == 2:
        host = sys.argv[1]
        print 'Default password is admin'
        passw = 'admin'
    else:
        print 'No host'
        exit()

    # Get cookie
    cookie = getCookie()
    print 'Authenticated, shell is up...'
    while 1:
        cmd = raw_input('> ')
        if cmd == 'exit' or cmd == 'quit':
            print 'Out...'
            exit()
        cmd = cmd.upper()
        send(cmd)

if __name__=='__main__':
    main()
